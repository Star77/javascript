/*
Nicely done, Souha.  Your JS application works great and
follows all the business requirements.  You've got everything
named correctly and the comments I needed to see are present
Nothing wrong here
20/20
*/
// javascript start here...

// function loadProvinces() includes the provArray[] that includes all provinces
function loadProvinces(){
    var provArray=["Alberta","British Columbia","Manitoba","New Brunswick"," Newfoundland and Labrador","Nova Scotia","Ontario","Prince Edward Island","Quebec","Saskatchewan"];
    var prov="<option value=''>-Select-</option>";

//for loop that loads all the provinces inside the provArray

    for(p=0;p<provArray.length;p++){
        prov+="<option value='"+provArray[p]+"'>"+provArray[p]+"</option>";
    }
    document.getElementById('cboProv').innerHTML=prov;
}

  // validateForm() to validate the user information and check them

function validateForm() {
    var name = document.getElementById('txtName').value;
    var email = document.getElementById('txtEmail').value;
    var province = document.getElementById('cboProv').value;

    //if the user miss entering the name this alert will popup

    if(name==""){
        alert('please enter your name');
        document.getElementById('txtName').focus();
        return false;
    }
    //if the user miss entering the email this alert will popup
    if(email==""){
        alert('please enter a valid email address');
        document.getElementById('txtEmail').focus();
        return false;
    }
    //if the user miss entering the Province this alert will popup

    if(province==""){
        alert('please select a province');
        document.getElementById('cboProv').focus();
        return false;
    }
  // if the user has all the information entering data this popup msg will shown

    alert('Your name is '+''+ name +','+' and you are from '+ '' + province);
}
