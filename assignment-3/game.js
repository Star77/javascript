// Well done here, Souha!  Your game works well and you've got
// all the functions and pieces named correctly.  The only
// issue I had was that you ask the user to enter in game pieces with
// capital letters, but you validate for game pieces with small letters.
// That's confusing to the user.  Great work otherwise!
// 18/20

// /**************************************
//  *  " Rock, Paper, Scissors " Game  *
//  ***************************************/

//array holds all the elements
var gamePieces = ["rock","paper","scissors","dynamite"];
//computer choice by random
var computerChoice = gamePieces[getRandomGamePiece(gamePieces.length)];
var userChoice;

//start function to start the game withthe prompt window so user can enter
//their choice

function start () {
  userChoice = prompt("Please enter your choice of Rock, Paper, Scissors or Dynamite");
  if (userChoice !== "rock" && userChoice !== "paper" && userChoice !== "scissors" && userChoice !== "dynamite"){
     alert("pls select !");
  } else {
     document.getElementById("choice").innerHTML = "You have chosen:    " + userChoice.toUpperCase();
     document.getElementById("start").disabled = true;
     document.getElementById("computerChoice").style.visibility = 'visible';
    }
 }

//random function to get value for the computer
function getRandomGamePiece(arrayLength){
  var rndChoice = Math.floor(Math.random() * arrayLength);
  return rndChoice;
}

 //function that compares the two choices together and return result
function whoWins(){
   document.getElementById("rnd-Choice").innerHTML = "Computer choice is:   " + computerChoice.toUpperCase();
   document.getElementById("computerChoice").disabled = true;
   document.getElementById("results").style.visibility = 'visible';
   if (userChoice === computerChoice){
     document.getElementById("results").innerHTML = "NOBODY WON! DRAW GAME!";

  } else if (userChoice === gamePieces[0] && computerChoice === gamePieces[1]){
     document.getElementById("results").innerHTML = "Computer Wins! play again"


  } else if (userChoice === gamePieces[0] && computerChoice === gamePieces[2]){
     document.getElementById("results").innerHTML = "Congratulations! \n YOU WON!!!"


  } else if (userChoice === gamePieces[0] && computerChoice === gamePieces[3]){
    document.getElementById("results").innerHTML = "Computer Wins! Try again"


 } else if (userChoice === gamePieces[1] && computerChoice === gamePieces[0]){
     document.getElementById("results").innerHTML = "Congratulations! \n YOU WON!!!"


  } else if (userChoice === gamePieces[1] && computerChoice === gamePieces[2]){
     document.getElementById("results").innerHTML = "Computer Wins! Try again"


  } else if (userChoice === gamePieces[1] && computerChoice === gamePieces[3]){
     document.getElementById("results").innerHTML = "Computer Wins! Try again"


  } else if (userChoice === gamePieces[2] && computerChoice === gamePieces[0]){
     document.getElementById("results").innerHTML = "Computer Wins! Try again"


  } else if (userChoice === gamePieces[2] && computerChoice === gamePieces[1]){
      document.getElementById("results").innerHTML = "Congratulations! \n YOU WIN!!!"


  } else if (userChoice === gamePieces[2] && computerChoice === gamePieces[3]){
      document.getElementById("results").innerHTML = "Congratulations! \n YOU WIN!!!"
      alert("That wasn't even fair! You win.")

   } else if (userChoice === gamePieces[3] && computerChoice === gamePieces[0]){
      document.getElementById("results").innerHTML = "Congratulations! \n YOU WIN!!!"


  } else if (userChoice === gamePieces[3] && computerChoice === gamePieces[1]){
      document.getElementById("results").innerHTML = "Congratulations! \n YOU WIN!!!"

   } else if (userChoice === gamePieces[3] && computerChoice === gamePieces[2]){
      document.getElementById("results").innerHTML = "Computer Wins! Try again"

   }

}

//return by Jquery
  $("#txt1").html(whoWins());
